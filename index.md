## Telldus Thermostat

Telldus live is not very good at controlling ovens because the allowed update rate is too slow for external control
and the triggers in my experience is not stable enough. 

Using Telldus TellStick ZNet local server API however does not have any limitation on how often you can call it.
This app will be able to control all kinds of ovens as long as it can be controlled by a device that can be connected to Telldus.

# Before you start
* You need to have Telldus TellStick ZNet v1 or v2
* Temperature sensors and switches that can turn of your oven
* A local server on your network that can always be on
* Some knowledge about docker and how to use docker compose

# Docker compose
Can be deployed in docker with the following docker-compose.yml:

```
version: '3'
services:
   server:
      image: "eidsa/termostat-server"
      environment:
         - "TELLDUS_URL=http://<your-telldus-ip/api"
         - "TELLDUS_BEARER-TOKEN=<your-telldus-token>"
         - "SERVER_PORT=80"
         - "SPRING_DATA_MONGODB_HOST=mongodb"
      expose:
         - 80
      restart: always
      depends_on:
         - mongodb
   client:
      image: "eidsa/termostat-client"
      ports:
         - 80:80
      restart: always
      depends_on:
         - server
   mongodb:
      image: mongo
      expose:
        - 27017
      restart: always
      volumes:
        - mongodb-data:/data/db
volumes:
   mongodb-data:
```

### Telldus bearer token
Currently it is not too easy to get the token as you need to do it manually. Description is located here: http://api.telldus.net/localapi/api.html

# Screenshots
![Image of Dashboard](https://eirikandre.github.io/telldus-thermostat/images/dashboard.png)

![Image of New room page](https://eirikandre.github.io/telldus-thermostat/images/new_room.png)

![Image of Edit the scheduler](https://eirikandre.github.io/telldus-thermostat/images/scheduler.png)

# Further plans
* Create UI to get Telldus token
* Create more user friendly way to create cron expressions
* Maybe add possibility to control other Telldus devices manually
